
*Project Name: Medical Portal*

## Description
This project is a Medical Portal that facilitates communication between students and doctors for medical assistance. It allows students to request medicines, doctors to review and fulfill those requests, and both parties to exchange messages regarding medical needs.

## Features
1. *Login System:*
   - Student Login
   - Doctor Login

2. *Student Dashboard:*
   - Request Medicine
   - Give Medicine
   - View Messages

3. *Doctor Dashboard:*
   - Check Requests
   - View History
   - Accept/Reject Requests

4. *Messaging System:*
   - Students and doctors can exchange messages regarding medical needs.

## Technologies Used
- Frontend:
  - Tkinter (Python GUI library)
- Backend:
  - Python (Tkinter for GUI, MySQL for database interactions)
  - MySQL (Database)

## Total Working
The system allows students to request medicines, doctors to review these requests, and communicate effectively regarding medical needs. The workflow involves:
1. *Student Flow:*
   - Login as a student
   - Choose from three options: Request Medicine, Give Medicine, View Messages
   - If "Request Medicine" is chosen, provide details of the medicine needed and submit the request.
   - If "Give Medicine" is chosen, see available requests, fill in details if medicine is available, and submit.
   - View and send messages.

2. *Doctor Flow:*
   - Login as a doctor
   - Check requests from students
   - View history of requests
   - Accept or reject requests based on history and availability.

## Contribution
This project was contributed to by:
- [Lavanya] - Frontend
- [Sreeja]- Frontend
- [Vyshanvi]-Frontend
- [Snehitha]-GitLab,Latex
 -[Akshaya]-Backend


## Installation
To run this project locally:
1. Clone the repository from GitLab.
2. Install MySQL and set up the database according to the schema provided.
3. Install Python and Tkinter library.
4. Modify the database connection settings in the Python code to connect to your MySQL database.
5. Run the Python script to launch the application.

## License
This project is licensed under the [Your License] License.